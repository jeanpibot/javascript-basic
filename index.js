let numeros = ['52$', '82$', '20$', '10$', '3$', '15$', '14$', '95$', '66$'];

// Map transforma o cambia una lista
const prices = numeros.map((numero) => Number(numero.slice(0, numero.length - 1)));

// Filter busca filtrar la lista y buscar dependiendo de la condición
const expensive = prices.filter((numero) => numero >= 20);

// reduce busca reducir la lista
const sum = expensive.reduce((acumulador, price) => acumulador + price);

console.log(sum);